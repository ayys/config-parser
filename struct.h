struct section_entity{
  char* key;
  void* value;
  struct section_entity *next;
};

struct section{
  char* name;
  struct section_entity *first_entity;
};
