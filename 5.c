#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_NAME_SIZE 50

typedef int state;

int main(int argc, char* argv[])
{
  FILE *input;
  char tmp_char;
  state section_change = 0;
  state section_name = 1;
  state section_body = 0;
  state section_key_value = 0;	/* 1 for value, 0 for key */
  /* temporary character holder */
  char *tmp  = malloc(2 * sizeof(char));
  tmp[1] = '\0';
  
  if (argc > 1)
    input = fopen(argv[1], "r");
  else return 1;
  
  while ((tmp_char = fgetc(input)) != EOF)
    {
      /* 2 consecutive new-lines denote change in section */
      if (tmp_char == '\n')
	{
	  if (section_change == 1)
	    /* section changed */
	    {
	      printf("------------------\n");
	      section_name = 1;
	      section_change = 0;
	    }
	  /* section did not change */
	  else
	    {
	      section_change = 1;
	    }
	}
      else if (section_name == 1 && tmp_char == '*')
	{
	  char* name = malloc(MAX_NAME_SIZE * sizeof(char));
	  memset(name, 0, MAX_NAME_SIZE * sizeof(char));

	  while ((tmp_char = fgetc(input)) != '\n')
	    {
	      tmp[0] = tmp_char;
	      strcat(name, tmp);
	    }
	  printf("Section Name: %s\n", name);
	  free(name);
	  section_body = 1;	/*  section body state when name is finished */
	}
      else if (section_body == 1)
	{
	  char* body_key = malloc(MAX_NAME_SIZE * sizeof(char));
	  char* body_value = malloc(MAX_NAME_SIZE * sizeof(char));
	  memset(body_key, 0, MAX_NAME_SIZE * sizeof(char));
	  memset(body_value, 0, MAX_NAME_SIZE * sizeof(char));

	  do
	    {
	      /* ignore whitespace */
	      if (!section_key_value && (tmp_char == ' ' || tmp_char == '\t'))
		continue;
	      if (tmp_char == '=')
		{
		  section_key_value = 1;
		  continue;
		}
	      tmp[0] = tmp_char;
	      if (section_key_value)
		strcat(body_value, tmp);
	      else
		strcat(body_key, tmp);		
	    } while  ((tmp_char = fgetc(input)) != '\n');
	  
	  printf("Key: %s\t", body_key);
	  printf("Value: %s\n", body_value);
	  free(body_key);
	  free(body_value);	  
	  section_body = 1;	/*  section body state when name is finished */
	  section_key_value = 0;
	}
      else
	{
	  section_change = 0;
	  section_name = 0;
	}
      printf("%c", tmp_char);
    }
  free(tmp);
  return 0;
}
