#include <stdio.h>


int main(int argc, char* argv[])
{
  FILE *input;
  char tmp_char;
  if (argc > 1)
    input = fopen(argv[1], "r");
  else return 1;
  
  while ((tmp_char = fgetc(input)) != EOF)
    {
      printf("%c", tmp_char);
    }
  return 0;
}
