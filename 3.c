#include <stdio.h>

typedef int state;

int main(int argc, char* argv[])
{
  FILE *input;
  char tmp_char;
  state section_change = 0;
  
  if (argc > 1)
    input = fopen(argv[1], "r");
  else return 1;
  
  while ((tmp_char = fgetc(input)) != EOF)
    {
      if (tmp_char == '\n')
	{
	  if (section_change == 1)
	    {
	      printf("------------------\n");
	      section_change = 0;
	    }
	  else
	    {
	      section_change = 1;
	    }
	}
      else
	{
	  section_change = 0;
	}
      printf("%c", tmp_char);
    }
  return 0;
}
